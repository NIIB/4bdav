# Compter le nombre total des enregistrements dans chaque table du schéma HR

SET SERVEROUTPUT ON;

DECLARE
    total_record INTEGER;
BEGIN
    FOR I IN (SELECT TABLE_NAME FROM USER_TABLES) 
        LOOP
            EXECUTE IMMEDIATE 'SELECT count(*) FROM ' || i.table_name INTO total_record;
            DBMS_OUTPUT.PUT_LINE( 'Table '  || i.table_name || ' : ' || total_record || ' lignes' );
        END LOOP;
    
    EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE ('Problème rencontré dans les comptages du schéma HR');
END;
/

# -----------------------------------------------------------------------------------------------------------------

# Compter le nombre d’employés dont la fonction (JOB) est MANAGER dans la table EMPLOYEE

SET SERVEROUTPUT ON;

DECLARE
     total_manager INTEGER;
BEGIN
    SELECT 
        count(*) INTO total_manager
    FROM 
        EMPLOYEES
    INNER JOIN JOBS
    ON employees.job_id = jobs.job_id
    WHERE JOBS.JOB_TITLE like '%Manager%';

    DBMS_OUTPUT.PUT_LINE ('Nombre de personnes qui sont manager : ' || total_manager);
END;

# Il y a 12 personnes qui sont manager

# -----------------------------------------------------------------------------------------------------------------

# Calculer la proportion (en pourcentage) de managers

SET SERVEROUTPUT ON;

DECLARE
     
     proportion_manager INTEGER;
     
BEGIN
    SELECT 
        ROUND(total_manager / (SELECT COUNT(*) FROM EMPLOYEES),4) * 100 INTO proportion_manager
    FROM (
            SELECT 
                count(*) AS total_manager
            FROM 
                EMPLOYEES
            INNER JOIN JOBS
            ON employees.job_id = jobs.job_id
            WHERE JOBS.JOB_TITLE like '%Manager%'
        );
    
    DBMS_OUTPUT.PUT_LINE ('Proportion de manager : ' || proportion_manager || ' %');
END; 

# Il y a 13% de manager

# -----------------------------------------------------------------------------------------------------------------

# Exécuter le programme et afficher le résultat de chaque requête à l’écran.

# Voir capture d'écran ci-joint

# -----------------------------------------------------------------------------------------------------------------
