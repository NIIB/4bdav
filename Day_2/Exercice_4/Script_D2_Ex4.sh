# Create schema

CREATE USER PILOTE IDENTIFIED BY oracle;
GRANT CREATE TABLE TO PILOTE;
GRANT CREATE SESSION TO PILOTE;

# -----------------------------------------------------------------------------------------------------------------

# Create table 

CREATE TABLE PILOTE(
    Matricule VARCHAR(255),
    Nom       VARCHAR(255),
    Ville     VARCHAR(255),
    Age       VARCHAR(255),
    Salaire   INTEGER
);

# -----------------------------------------------------------------------------------------------------------------

# Add tablespace 

GRANT UNLIMITED TABLESPACE TO PILOTE;

# -----------------------------------------------------------------------------------------------------------------

# Ajout d'élément dans la table

INSERT INTO PILOTE VALUES('001','Dupond','PARIS','45',5000);
INSERT INTO PILOTE VALUES('002','Tintin','LYON','50',5499);
INSERT INTO PILOTE VALUES('003','Milou','MARSEILLE','55',7855);
INSERT INTO PILOTE VALUES('004','JACK','LILLE','60',6589);

# -----------------------------------------------------------------------------------------------------------------

# Écrire un programme permettant de calculer la moyenne des salaires des pilotes dont l’âge est entre 45 et 55 ans.

SET SERVEROUTPUT ON;

DECLARE
    avg_salary pilote.salaire%type;
BEGIN
    SELECT 
        AVG(salaire) INTO avg_salary
    FROM pilote
    WHERE age >= 45 and age <= 55;
    DBMS_OUTPUT.PUT_LINE ('La moyenne des salaires mensuels pour les pilotes entre 45 et 55 ans est de : ' || avg_salary || '€');

END;
/

# -----------------------------------------------------------------------------------------------------------------

# Écrire un programme permettant de calculer le salaire annuel d'un pilote
# Création d'une fonction permettant de retourner le salaire annuel calculer

CREATE OR REPLACE FUNCTION f_retour_annual_salary (p_matricule pilote.matricule%TYPE)
RETURN NUMBER
    AS annual_salary pilote.salaire%TYPE;
BEGIN
    SELECT salaire * 12 INTO annual_salary
FROM pilote
WHERE p_matricule = matricule;
RETURN annual_salary;

END;
/

# Requête permettant d'utiliser la fonction précédente et d'affichier matricule, nom et salaire annuel des pilotes

SELECT
    matricule,
    nom,
    f_retour_annual_salary(matricule) salaire_annuel
FROM
    pilote;

# -----------------------------------------------------------------------------------------------------------------