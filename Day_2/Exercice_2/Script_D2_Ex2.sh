SET SERVEROUTPUT ON;

DECLARE

BEGIN
  FOR i IN (
        SELECT 
            object_name, 
            object_type
        FROM 
            user_objects 
    )
  LOOP
     dbms_output.put_line('L''objet '|| i.object_name ||' est du type ' || i.object_type );
  END LOOP;
END;

# -----------------------------------------------------------------------------------------------------------------