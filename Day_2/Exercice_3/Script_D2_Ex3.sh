# Create schema

CREATE USER VOL IDENTIFIED BY oracle;
GRANT CREATE TABLE TO VOL;
GRANT CREATE SESSION TO VOL;

# -----------------------------------------------------------------------------------------------------------------

# Create table 

CREATE TABLE VOL(
    IdVol                  VARCHAR(255),
    Date_heure_depart      VARCHAR(255),
    Date_heure_arrivee     VARCHAR(255),
    Ville_depart           VARCHAR(255),
    Ville_arrivee          VARCHAR(255)
);

# -----------------------------------------------------------------------------------------------------------------

# Add tablespace 

GRANT UNLIMITED TABLESPACE TO VOL;

# -----------------------------------------------------------------------------------------------------------------



# Écrire un programme qui permet d'insérer le vol BA270 partant de Rome à 10h15 et arrivant
# à Paris à 12h15

SET SERVEROUTPUT ON;

DECLARE
    myVol                 VARCHAR(255) := 'BA270';
    my_Date_heure_depart  VARCHAR(255) := '10h15';
    my_Date_heure_arrivee VARCHAR(255) := '12h15';
    my_Ville_depart       VARCHAR(255) := 'Rome' ; 
    my_Ville_arrivee      VARCHAR(255) := 'Paris';

BEGIN
    INSERT INTO VOL 
        (IdVol, Date_heure_depart, Date_heure_arrivee, Ville_depart, Ville_arrivee)
    VALUES 
        (myVol, my_Date_heure_depart, my_Date_heure_arrivee, my_Ville_depart, my_Ville_arrivee);
    COMMIT;
    DBMS_OUTPUT.PUT_LINE ('Le vol : ' || myVol || ' partant de : '|| my_Ville_depart || ' à '  || my_date_heure_depart  || ' et pour une arrivée à ' || my_Ville_arrivee ||' à ' || my_date_heure_arrivee ||' a été inséré');

END;
/

# -----------------------------------------------------------------------------------------------------------------
