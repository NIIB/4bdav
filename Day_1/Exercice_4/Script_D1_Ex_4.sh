# Proposition d'un MCD (Entités + Attriuts) 

Table Hotel {
  hotel_id UUID [pk]
  stars INT
  adresse VARCHAR(255)
}

Table Room {
  room_id UUID [pk]
  capacity INT
  standing INT
  price INT
  equipment VARCHAR(255)
  category_type INT
}

# -----------------------------------------------------------------------------------------------------------------

# Relation 
# Un chambre appartiendra à un hotel et un hotel pourra avoir plusieurs chambres
# Cardinalités : 
# Hotel   => Chambre (1,N)
# Chambre => Hotel   (1,1)

ref : Hotel.hotel_id < Room.room_id

# -----------------------------------------------------------------------------------------------------------------

# Le model MCD/MLD est disponible dans le projet sous  DAY_1/Exercice_4/MCS-MLD.png

# -----------------------------------------------------------------------------------------------------------------

# Creation du shema hotel

# Create schema

CREATE USER hotel IDENTIFIED BY oracle;
GRANT CREATE TABLE TO hotel;
GRANT CREATE SESSION TO hotel;

# -----------------------------------------------------------------------------------------------------------------

# Create all tables 

CREATE TABLE Hotel(
  hotel_id INT PRIMARY KEY NOT NULL,
  stars INT,
  adresse VARCHAR(255)
);

CREATE TABLE Room(
  room_id INT PRIMARY KEY NOT NULL,
  capacity INT,
  standing INT,
  price INT,
  equipment VARCHAR(255),
  category_type INT,
  hotel_id INT
);

# Add Constraint

ALTER TABLE Room
ADD CONSTRAINT FK_hotel_ID
FOREIGN KEY (hotel_id) 
REFERENCES Hotel(hotel_id);

# -----------------------------------------------------------------------------------------------------------------

# Le model MPD est disponible dans le projet sous  DAY_1/Exercice_4/MPD_Hotel.png

# -----------------------------------------------------------------------------------------------------------------