from pymongo import MongoClient
import urllib.parse
import certifi

username = urllib.parse.quote_plus('nicolas')
password = urllib.parse.quote_plus("")

uri = "mongodb+srv://{}:{}@cluster0.leremoy.mongodb.net/?retryWrites=true&w=majority".format(username, password)
client = MongoClient(uri,tlsCAFile=certifi.where())

db = client.dblp
coll = db.publis

cursor_1 = coll.find({"type" : "Book"})
for result_object in cursor_1:
    print(result_object)   

#cursor_2 = coll.find({'year' : {'$gte' : 2011}})
#for result_object in cursor_2:
#    print(result_object)   

#cursor_3 = coll.find({"type" : "Book", "year" : {"$gte" : 2014}})
#for result_object in cursor_3:
#    print(result_object) 

#cursor_4 = coll.find({"authors" : "Toru Ishida"})
#for result_object in cursor_4:
#    print(result_object) 

#cursor_5 = coll.distinct("publisher")
#for result_object in cursor_5:
#    print(result_object) 

#cursor_6 = coll.distinct("authors")
#for result_object in cursor_6:
#    print(result_object) 

#cursor_7 = coll.aggregate([{"$match":{"authors" : "Toru Ishida"}}, { "$sort" : { "booktitle" : 1, "pages.start" : 1 } }])
#for result_object in cursor_7:
#    print(result_object) 

#cursor_8 = coll.aggregate([{"$match":{"authors" : "Toru Ishida"}}, {"$sort" : { "booktitle" : 1, "pages.start" : 1 }}, {"$project" : {"title" : 1, "pages" : 1}}])
#for result_object in cursor_8:
#    print(result_object) 

#cursor_9 = coll.aggregate([{"$match":{"authors" : "Toru Ishida"}}, {"$group":{"_id":"null", "total" : { "$sum" : 1}}}])
#for result_object in cursor_9:
#    print(result_object) 

#cursor_10 = coll..aggregate([{"$match":{"year" : {"$gte" : 2011}}}, {"$group":{"_id":"$type", total : { "$sum" : 1}}}])
#for result_object in cursor_10:
#    print(result_object) 

#cursor_11 = coll.aggregate([{ "$unwind" : "$authors" }, { "$group" : { "_id" : "$authors", "number" : { "$sum" : 1 } }}, {"$sort" : {"number" : -1}}] )
#for result_object in cursor_11:
#    print(result_object) 

client.close();