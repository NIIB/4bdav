##############################################

#Lancement du shell HBASE
hbase shell

##############################################

#Lister les tables de hbase
hbase > list

##############################################

#Creation de la table dans HBASE
hbase> create 'sales _ledger','customer','sales'

##############################################

#Vérification de la création de la table
hbase > list

##############################################

# Insertion des données
put 'sales_ledger','101','customer:name', 'John White'
put 'sales_ledger','101','customer:city', 'Los Angeles, CA'
put 'sales_ledger','101','sales:product', 'Chairs'
put 'sales_ledger','101','sales:amount', '$400.00'

##############################################

#Lecture des données

scan 'sales_ledger'
